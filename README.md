База данных интернет-магазина. База данных состоит из 5 таблиц

1.otus_Client - клиенты

2.otus_Article - товары

3.otus_Price - цены на товары с датами изменения

4.otus_Order - заказы

5.otus_Store - приход/расход товаров на складе

В библиотеках реализованы функции:

1.void ToStore(string date1, int idArticle, int amount) приход товара на склад date1-дата поступления, idArticle-id товара, amount-количество единиц товара.

2.double PriceToDate(string date1, Int32 idArticle) цена товара на дату

3.bool IsThereArticle(string date1, Int32 idArticle, Int32 amount) достаточно ли товара на дату, в зависимости от заказанного количества

4.string StoreADay(string date1) остатки товаров на складе на дату

5.string BestClient() клиент, заказавший на наибольшую сумму

6.double TotalOrder(Int32 idOrder) сумма заказа