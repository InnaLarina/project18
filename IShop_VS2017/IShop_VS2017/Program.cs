﻿using System;





namespace IShop_VS2017
{
    class Program
    {
        static void Main(string[] args)
        {
            DBQueryLibraryEF1.DBQuery dbquery = new DBQueryLibraryEF1.DBQuery();
            if (dbquery.IsThereArticle("02.05.2019", 1, 2018))
                Console.WriteLine("There is enough TV Sets.");
            else
                Console.WriteLine("There is not enough TV Sets.");
            Console.WriteLine();
            Console.WriteLine("02.05.2019. TVSet cost " + dbquery.PriceToDate("02.05.2019", 1).ToString());
            Console.WriteLine();
            Console.WriteLine("order1 costs " + dbquery.TotalOrder(1).ToString()+" RUR.");
            Console.WriteLine();
            Console.WriteLine("Arrival of the 8 coffeemakers to the store ");
            dbquery.ToStore("21.12.2019", 3, 8);
            Console.WriteLine();
            Console.WriteLine(dbquery.StoreADay("01.12.2019"));
            Console.WriteLine();
            Console.WriteLine("Best client: " + dbquery.BestClient());
            Console.WriteLine();
            Console.WriteLine("Let's add a client : Fermino");
            dbquery.CreateClient(5,"Fermino","Fermino",@"Fermino@mail.ru");
            Console.ReadKey();
        }
    }
}
