﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryAdoNet
{
    interface IDBQuery
    {
        void DeleteClient(int pid);
        void CreateClient(int pid, string pFirstName, string pLastName, string pEmail);
        string ReadClient(int pid);
        void UpdateClient(int pid, string pFirstName, string pLastName, string pEmail);
        void DeleteArticle(int pid);
        void CreateArticle(int pid, string pNameItem, string pColor, string pProducer);
        string ReadArticle(int pid);
        void UpdateArticle(int pid, string pNameItem, string pColor, string pProducer);
        void DeleteOrder(int pid);
        void CreateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient);
        string ReadOrder(int pid);
        void UpdateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient);
        void DeletePrice(int pid);
        void CreatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle);
        string ReadPrice(int pid);
        void UpdatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle);
        void DeleteStore(int pid);
        void CreateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount);
        string ReadStore(int pid);
        void UpdateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount);
        double PriceToDate(string date1, Int32 idArticle);
        bool IsThereArticle(string date1, Int32 idArticle, Int32 amount);
        double TotalOrder(Int32 idOrder);
        void ToStore(string date1, int idArticle, int amount);
        string StoreADay(string date1);
        string BestClient();
    }
}
