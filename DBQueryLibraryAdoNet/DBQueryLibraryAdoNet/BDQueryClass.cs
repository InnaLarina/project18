﻿using System;
using System.Data.SqlClient;


namespace DBQueryLibraryAdoNet
{
    public class DBQuery: IDBQuery
    {
        private const string sqlConnectionString = "Data Source=elma-td1;Initial Catalog=ELMA_base;User=;Password=;";
        public double PriceToDate(string date1, Int32 idArticle)
        {
            double price = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT TOP (1) [Id],[IdArticle],[DateIzm],[PriceArticle] " +
                                     String.Format("FROM [Views].[dbo].[otus_Price] where IdArticle = {0} and DateIzm<=convert(datetime,'{1}',104) order by DateIzm desc", idArticle, date1);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return -1;
                    while (reader.Read())
                    {

                        price = Convert.ToDouble(reader.GetValue(3));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return price;
        }
        public bool IsThereArticle(string date1, Int32 idArticle, Int32 amount)
        {
            double amountFound = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT isnull(sum(Amount),0)  " +
                                     String.Format("FROM [Views].[dbo].[otus_Store] where IdArticle = {0} and DateInStore<=convert(datetime,'{1}',104) ", idArticle, date1);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        amountFound = Convert.ToDouble(reader.GetValue(0));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return amountFound >= amount;
        }

        public double TotalOrder(Int32 idOrder)
        {
            double total = 0;
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = "SELECT isnull(sum([Amount]*[Views].[dbo].[otus_Price_Article](DateOrder,IdArticle)),0) " +
                                     "FROM [Views].[dbo].[otus_Order] " +
                                     String.Format("WHERE NOrder = {0} ", idOrder);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        total = Convert.ToDouble(reader.GetValue(0));

                    }
                    reader.Close();
                };
                connection.Close();
            }
            return total;
        }
        public void ToStore(string date1, int idArticle, int amount)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string insertQuery = String.Format("insert into [Views].[dbo].[otus_Store] values(convert(datetime,'{0}',104),{1},{2})", date1, idArticle, amount);
                using (SqlCommand insertCommand = new SqlCommand(insertQuery, connection))
                {
                    var res = insertCommand.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public string StoreADay(string date1)
        {
            string resultString = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = "select a.NameItem,sum(s.Amount) Amount from [Views].[dbo].[otus_Article] a " +
                                     "inner join [Views].[dbo].[otus_Store] s on a.id = s.idArticle " +
                                     String.Format("where s.DateInStore<=convert(datetime,'{0}',104) ", date1) +
                                     "group by a.NameItem ";
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows) return "There is none";
                    while (reader.Read())
                    {

                        resultString += reader.GetValue(0).ToString() + ": " + reader.GetValue(1).ToString() + "\r\n";

                    }
                    reader.Close();
                };
                connection.Close();
                return "На " + date1 + " на складе: \r\n" + resultString;
            }
        }

        public string BestClient()
        {
            string client = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {

                connection.Open();
                string selectQuery = "SELECT top(1) c.FirstName+' '+c.LastName ,sum([Amount]*[Views].[dbo].[otus_Price_Article](DateOrder,IdArticle)) " +
                                     "FROM [Views].[dbo].[otus_Order] o " +
                                     "inner join [Views].[dbo].[otus_Client] c on o.IdClient = c.Id " +
                                     "group by c.FirstName,c.LastName " +
                                     "order by 2 desc ";
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "There is none";
                    while (reader.Read())
                    {

                        client = reader.GetValue(0).ToString();

                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return client;
        }
        public void DeleteClient(int pid) 
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = String.Format("delete from [Views].[dbo].[otus_Client] where id = {0}",pid);
                SqlCommand command = new SqlCommand(sqlString,connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        public void UpdateClient(int pid, string pFirstName, string pLastName, string pEmail) 
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "update [Views].[dbo].[otus_Client] " +
                                   String.Format(" set FirstName = {0},", pFirstName) +
                                   String.Format(" set LastName = {0},", pLastName) +
                                   String.Format(" set Email = {0},", pEmail) +
                                   String.Format(" where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        public void CreateClient(int pid, string pFirstName, string pLastName, string pEmail) 
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "insert into [Views].[dbo].[otus_Client] (id, FirstName, LastName, Email)" +
                                   String.Format(" values({0},{1},{2},{3}", pid, pFirstName, pLastName, pEmail);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
        public string ReadClient(int pid) 
        {
            string result = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = String.Format("SELECT top(1) id, FirstName, LastName, Email FROM [Views].[dbo].[otus_Client] c where id = {0} ", pid);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "";
                    while (reader.Read())
                    {

                        result = String.Format("Client {0}: {1} {2}, email: {3}", reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString());
                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return result;
        }

        public void DeleteArticle(int pid)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = String.Format("delete from [Views].[dbo].[otus_Article] where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void CreateArticle(int pid, string pNameItem, string pColor, string pProducer)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "insert into [Views].[dbo].[otus_Article] (id, NameItem, Color, Producer)" +
                                   String.Format(" values({0},{1},{2},{3}", pid, pNameItem, pColor, pProducer);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public string ReadArticle(int pid)
        {
            string result = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = String.Format("SELECT top(1) id,NameItem,Color,Producer FROM [Views].[dbo].[otus_Article] c where id = {0} ", pid);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "";
                    while (reader.Read())
                    {

                        result = String.Format("Article {0}: {1} Color: {2}, Producer: {3}", reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString());
                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return result;
        }

        public void UpdateArticle(int pid, string pNameItem, string pColor, string pProducer)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "update [Views].[dbo].[otus_Article] " +
                                   String.Format(" set NameItem = {0},", pNameItem) +
                                   String.Format(" set Color = {0},", pColor) +
                                   String.Format(" set Producer = {0},", pProducer) +
                                   String.Format(" where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void DeleteOrder(int pid)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = String.Format("delete from [Views].[dbo].[otus_Order] where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void CreateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "insert into [Views].[dbo].[otus_Order] (id, NOrder, DateOrder,idArticle, Amount, idClient)" +
                                   String.Format(" values({0},{1},{2},{3}", pid, pNOrder, pDateOrder,pidArticle, pAmount, pidClient);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public string ReadOrder(int pid)
        {
            string result = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = String.Format("SELECT top(1) Id,NOrder,DateOrder,idArticle,Amount,idClient FROM [Views].[dbo].[otus_Order] c where id = {0} ", pid);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "";
                    while (reader.Read())
                    {

                        result = String.Format("Order {0}: №Order: {1}, DateOrder: {2}, IdArticle: {3}, Amount: {4}, idClient: {5}", reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString());
                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return result;
        }

        public void UpdateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "update [Views].[dbo].[otus_Order] " +
                                   String.Format(" set NOrder = {0},", pNOrder) +
                                   String.Format(" set DateOrder = {0},", pDateOrder) +
                                   String.Format(" set idArticle = {0},", pidArticle) +
                                   String.Format(" set Amount = {0},", pAmount) +
                                   String.Format(" set idClient = {0},", pidClient) +
                                   String.Format(" where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void DeletePrice(int pid)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = String.Format("delete from [Views].[dbo].[otus_Price] where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void CreatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "insert into [Views].[dbo].[otus_Price] (id, idArticle, DateIzm, PriceArticle)" +
                                   String.Format(" values({0},{1},{2},{3}", pid, pidArticle, pDateIzm, pPriceArticle);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public string ReadPrice(int pid)
        {
            string result = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = String.Format("SELECT top(1) Id,idArticle,dateIzm,PriceArticle FROM [Views].[dbo].[otus_Price] c where id = {0} ", pid);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "";
                    while (reader.Read())
                    {

                        result = String.Format("Price {0}: Article: {1}, DateIzm: {2}, PriceArticle: {3}", reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString());
                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return result;
        }

        public void UpdatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "update [Views].[dbo].[otus_Price] " +
                                   String.Format(" set idArticle = {0},", pidArticle) +
                                   String.Format(" set DateIzm = {0},", pDateIzm) +
                                   String.Format(" set PriceArticle = {0},", pPriceArticle) +
                                   String.Format(" where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void DeleteStore(int pid)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = String.Format("delete from [Views].[dbo].[otus_Store] where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void CreateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "insert into [Views].[dbo].[otus_Store] (id, DateInStore, idArticle, Amount)" +
                                   String.Format(" values({0},{1},{2},{3}", pid, pDateInStore, pidArticle, pAmount);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public string ReadStore(int pid)
        {
            string result = "";
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string selectQuery = String.Format("SELECT top(1) Id,DateInStore,idArticle,Amount FROM [Views].[dbo].[otus_Store] c where id = {0} ", pid);
                using (SqlCommand cmd = new SqlCommand(selectQuery, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return "";
                    while (reader.Read())
                    {

                        result = String.Format("Store {0}: Article: {1}, DateInStore: {2}, Amount: {3}", reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString());
                    }
                    reader.Close();
                }
                ;
                connection.Close();
            }
            return result;
        }

        public void UpdateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount)
        {
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                connection.Open();
                string sqlString = "update [Views].[dbo].[otus_Price] " +
                                   String.Format(" set idArticle = {0},", pidArticle) +
                                   String.Format(" set DateInStore = {0},", pDateInStore) +
                                   String.Format(" set Amount = {0},", pAmount) +
                                   String.Format(" where id = {0}", pid);
                SqlCommand command = new SqlCommand(sqlString, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}
