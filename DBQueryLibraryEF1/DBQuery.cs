﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    public class DBQuery: IDBQuery
    {
        public void ToStore(string date1, int idArticle, int amount)
        {
            
            using (otus_StoreContext db = new otus_StoreContext())
            {
                // создаем объект otus_Store
                otus_Store store1 = new otus_Store { DateInStore = DateTime.Parse(date1), idArticle = idArticle, Amount = amount };

                // добавляем их в бд
                db.otus_Stories.Add(store1);

                db.SaveChanges();
            }
        }
        public void ToUser(string name, int age)
        {
            using (otus_UserContext db = new otus_UserContext())
            {
                // создаем объект otus_Store
                otus_User user = new otus_User { Name = name, Age = age};

                // добавляем их в бд
                db.otus_Users.Add(user);

                db.SaveChanges();
            }
        }
        public double PriceToDate(string date1, Int32 idArticle)
        {
            double price = 0;
            DateTime pdate = DateTime.Parse(date1);
            using (otus_PriceContext db = new otus_PriceContext())
            {
                var prices = db.otus_Prices;
                var lastPrice = (from pr in prices
                                 where pr.idArticle == idArticle && pr.dateIzm <= pdate
                                 select pr).OrderByDescending(pr => pr.dateIzm).Take(1);
                foreach (var l in lastPrice)
                {
                    otus_Price lastPrise = (otus_Price)l;
                    price = lastPrise.PriceArticle;
                }

            }
            return price;
        }
        public bool IsThereArticle(string date1, Int32 idArticle, Int32 amount)
        {
            double amountFound = 0;
            DateTime pdate = DateTime.Parse(date1);
            using (otus_StoreContext db = new otus_StoreContext())
            {
                var stores = db.otus_Stories;
                amountFound = (from s in stores
                                 where s.idArticle == idArticle && s.DateInStore <= pdate
                                 select s).Sum(s=>s.Amount);
             }
            return amountFound >= amount;
        }
        public string StoreADay(string date1)
        {
            string resultString = "";
            DateTime pdate = DateTime.Parse(date1);
            using (otus_StoreContext dbs = new otus_StoreContext())
            using (otus_ArticleContext dba = new otus_ArticleContext())
            {
                var stores = dbs.otus_Stories;
                var articles = dba.otus_Articles;
                var list_articles = articles.AsEnumerable();
                var groups = list_articles.Join(stores,
                                       a => a.Id,
                                       s => s.idArticle,
                                       (a, s) => new
                                       { a.NameItem, s.DateInStore, s.Amount }).Where(s => s.DateInStore <= pdate).GroupBy(s => s.NameItem).Select(g => new
                                       {
                                           article = g.Key,
                                           total = g.Sum(x => x.Amount)
                                       });
                foreach (var gr in groups)
                {
                    resultString += gr.article + ": " + gr.total.ToString() + "\r\n";
                }

            }
            return "На " + date1 + " на складе: \r\n" + resultString;
        }
        public string BestClient()
        {
            string client = "";
            using (otus_ClientContext dbc = new otus_ClientContext())
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                var clients = dbc.otus_Clients;
                var list_clients = clients.AsEnumerable();
                var bestClient = list_clients.Join(orders,
                                       c => c.Id,
                                       o => o.idClient,
                                       (c, o) => new
                                       { fioClient = c.FirstName+" "+c.LastName,
                                         total = o.Amount * Price_Article(o.DateOrder, o.idArticle) })
                                         .Select(c=>c)
                                         .GroupBy(s => s.fioClient)
                                         .Select(g => new
                                       {
                                           fioClient = g.Key,
                                           ttl = g.Sum(x => x.total)
                                       })
                                       .OrderByDescending(x=>x.ttl)
                                       .Take(1);
                foreach (var cl in bestClient)
                {
                    client = cl.fioClient;
                }
            }
            return client;
        }
        public double Price_Article(DateTime date1, int idArticle)
        {
            double price = 0;
            using (otus_PriceContext db = new otus_PriceContext())
            {
                var prices = db.otus_Prices;
                var lastPrice = (from s in prices
                               where s.idArticle == idArticle && s.dateIzm <= date1
                               select s).OrderByDescending(s=>s.dateIzm).Take(1);
                foreach (var pr in lastPrice)
                {
                    price = pr.PriceArticle;
                }
            }
            return price;
        }
        public double TotalOrder(Int32 idOrder)
        {
            double total = 0;
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                var list_orders = orders.AsEnumerable();
                total = list_orders.Where(o => o.NOrder == idOrder).Select(o => new { total = o.Amount * Price_Article(o.DateOrder, o.idArticle) }).Sum(o => o.total);
            }
            return total;
        }
        public void DeleteClient(int pid)
        {
            using (otus_ClientContext dbo = new otus_ClientContext())
            {
                var clients = dbo.otus_Clients;
                otus_Client client = clients
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (client != null)
                {
                    clients.Remove(client);
                    dbo.SaveChanges();
                }
            }
        }
        public void CreateClient(int pid, string pFirstName, string pLastName, string pEmail) 
        {
            using (otus_ClientContext dbo = new otus_ClientContext())
            {
                otus_Client client = new otus_Client
                {
                    Id = pid,
                    FirstName = pFirstName,
                    LastName = pLastName,
                    Email = pEmail
                };
                dbo.otus_Clients.Add(client);
                dbo.SaveChanges();
            }
        }
        public string ReadClient(int id) 
        {
            string result = "";
            using (otus_ClientContext dbo = new otus_ClientContext())
            {
                var clients = dbo.otus_Clients;
                otus_Client client = clients
                .Where(o => o.Id == id)
                .FirstOrDefault();
                if (client != null) result = String.Format("Client {0}: {1} {2}, email: {3}",client.Id, client.FirstName,client.LastName,client.Email);
            }
            return result;
        }
        public void UpdateClient(int pid, string pFirstName, string pLastName, string pEmail) 
        {
            using (otus_ClientContext dbo = new otus_ClientContext())
            {
                var clients = dbo.otus_Clients;
                otus_Client client = clients
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (client != null)
                {
                    client.FirstName = pFirstName;
                    client.LastName = pLastName;
                    client.Email = pEmail;
                    dbo.SaveChanges();
                }
            }
        }

        public void DeleteArticle(int pid)
        {
            using (otus_ArticleContext dbo = new otus_ArticleContext())
            {
                var articles = dbo.otus_Articles;
                otus_Article article = articles
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (article != null)
                {
                    articles.Remove(article);
                    dbo.SaveChanges();
                }
            }
        }

        public void CreateArticle(int pid, string pNameItem, string pColor, string pProducer)
        {
            using (otus_ArticleContext dbo = new otus_ArticleContext())
            {
                otus_Article article = new otus_Article
                {
                    Id = pid,
                    NameItem = pNameItem,
                    Color = pColor,
                    Producer = pProducer
                };
                dbo.otus_Articles.Add(article);
                dbo.SaveChanges();
            }
        }

        public string ReadArticle(int pid)
        {
            string result = "";
            using (otus_ArticleContext dbo = new otus_ArticleContext())
            {
                var articles = dbo.otus_Articles;
                otus_Article client = articles
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (client != null) result = String.Format("Article {0}: {1} Color: {2}, Producer: {3}", client.Id, client.NameItem, client.Color, client.Producer);
            }
            return result;
        }

        public void UpdateArticle(int pid, string pNameItem, string pColor, string pProducer)
        {
            using (otus_ArticleContext dbo = new otus_ArticleContext())
            {
                var articles = dbo.otus_Articles;
                otus_Article article = articles
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (article != null)
                {
                    article.NameItem = pNameItem;
                    article.Color = pColor;
                    article.Producer = pProducer;
                    dbo.SaveChanges();
                }
            }
        }

        public void DeleteOrder(int pid)
        {
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                otus_Order order = orders
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (order != null)
                {
                    orders.Remove(order);
                    dbo.SaveChanges();
                }
            }
        }

        public void CreateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient)
        {
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                otus_Order order = new otus_Order
                {
                    Id = pid,
                    NOrder = pNOrder,
                    idArticle = pidArticle,
                    Amount = pAmount,
                    idClient = pidClient
                };
                dbo.otus_Orders.Add(order);
                dbo.SaveChanges();
            }
        }

        public string ReadOrder(int pid)
        {
            string result = "";
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                otus_Order order = orders
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (order != null) result = String.Format("Order {0}: №Order: {1}, DateOrder: {2}, IdArticle: {3}, Amount: {4}, idClient: {5}", order.Id, order.NOrder, order.DateOrder, order.idArticle,order.Amount, order.idClient);
            }
            return result;
        }

        public void UpdateOrder(int pid, int pNOrder, DateTime pDateOrder, int pidArticle, int pAmount, int pidClient)
        {
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                otus_Order order = orders
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (order != null)
                {
                    order.NOrder = pNOrder;
                    order.DateOrder = pDateOrder;
                    order.idArticle = pidArticle;
                    order.Amount = pAmount;
                    order.idClient = pidClient;
                    dbo.SaveChanges();
                }
            }
        }

        public void DeletePrice(int pid)
        {
            using (otus_PriceContext dbo = new otus_PriceContext())
            {
                var prices = dbo.otus_Prices;
                otus_Price price = prices
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (price != null)
                {
                    prices.Remove(price);
                    dbo.SaveChanges();
                }
            }
        }

        public void CreatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle)
        {
            using (otus_PriceContext dbo = new otus_PriceContext())
            {
                otus_Price price = new otus_Price
                {
                    Id = pid,
                    idArticle = pidArticle,
                    dateIzm = pDateIzm,
                    PriceArticle = pPriceArticle
                };
                dbo.otus_Prices.Add(price);
                dbo.SaveChanges();
            }
        }

        public string ReadPrice(int pid)
        {
            string result = "";
            using (otus_PriceContext dbo = new otus_PriceContext())
            {
                var prices = dbo.otus_Prices;
                otus_Price price = prices
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (price != null) result = String.Format("Order {0}: Article: {1}, DateIzm: {2}, PriceArticle: {3}", price.Id, price.idArticle, price.dateIzm, price.PriceArticle);
            }
            return result;
        }

        public void UpdatePrice(int pid, int pidArticle, DateTime pDateIzm, double pPriceArticle)
        {
            using (otus_PriceContext dbo = new otus_PriceContext())
            {
                var prices = dbo.otus_Prices;
                otus_Price price = prices
                .Where(o => o.Id == pid)
                .FirstOrDefault();
                if (price != null)
                {
                    price.PriceArticle = pPriceArticle;
                    price.dateIzm = pDateIzm;
                    price.idArticle = pidArticle;
                    dbo.SaveChanges();
                }
            }
        }

        public void DeleteStore(int pid)
        {
            using (otus_StoreContext dbo = new otus_StoreContext())
            {
                var stories = dbo.otus_Stories;
                otus_Store store = stories
                .Where(o => o.id == pid)
                .FirstOrDefault();
                if (store != null)
                {
                    stories.Remove(store);
                    dbo.SaveChanges();
                }
            }
        }

        public void CreateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount)
        {
            using (otus_StoreContext dbo = new otus_StoreContext())
            {
                otus_Store store = new otus_Store
                {
                    id = pid,
                    DateInStore = pDateInStore,
                    idArticle = pidArticle,
                    Amount = pAmount
                };
                dbo.otus_Stories.Add(store);
                dbo.SaveChanges();
            }
        }

        public string ReadStore(int pid)
        {
            string result = "";
            using (otus_StoreContext dbo = new otus_StoreContext())
            {
                var stories = dbo.otus_Stories;
                otus_Store store = stories
                .Where(o => o.id == pid)
                .FirstOrDefault();
                if (store != null) result = String.Format("Store {0}: Article: {1}, DateInStore: {2}, Amount: {3}", store.id, store.idArticle, store.DateInStore, store.Amount);
            }
            return result;
        }

        public void UpdateStore(int pid, DateTime pDateInStore, int pidArticle, int pAmount)
        {
            using (otus_StoreContext dbo = new otus_StoreContext())
            {
                var stories = dbo.otus_Stories;
                otus_Store store = stories
                .Where(o => o.id == pid)
                .FirstOrDefault();
                if (store != null)
                {
                    store.Amount = pAmount;
                    store.DateInStore = pDateInStore;
                    store.idArticle = pidArticle;
                    dbo.SaveChanges();
                }
            }
        }
    }
}
