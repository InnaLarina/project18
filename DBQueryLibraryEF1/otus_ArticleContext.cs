﻿using System.Data.Entity;

namespace DBQueryLibraryEF1
{
    class otus_ArticleContext : DbContext
    {
        public otus_ArticleContext():base("DBConnection")
        {}
        public DbSet<otus_Article> otus_Articles { get; set; }
    }
}
