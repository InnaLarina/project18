﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    public class otus_Store
    {
        public int id { get; set; }
        public DateTime DateInStore { get; set; }
        public int idArticle { get; set; }
        public int Amount { get; set; }
    }
}
