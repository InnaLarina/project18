﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DBQueryLibraryEF1
{
    class otus_StoreContext : DbContext
    {
        public otus_StoreContext(): base("DBConnection")
            {}
        public DbSet<otus_Store> otus_Stories { get; set; }
    }
}
